#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct list {
    void *obj;
    struct list *next;
    struct list *prev;
};

struct list *list_append(struct list *list, void *obj)
{
    if (list) {
        while (list->next)
            list = list->next;
        list->next = calloc(1, sizeof(struct list));
        list = list->next;
    } else
        list = calloc(1, sizeof(struct list));
    list->obj = obj;
    return list;

}

void foreach(struct list *list, void (*op)(void *))
{
    if (list) {
        op(list->obj);
        while ((list = list->next))
            op(list->obj);
    }
}

void op(void *a)
{
    puts(a);
}

int main()
{
    struct list *list = list_append(0, "AAA");
    list_append(list, "РУССКИЕ БУКАВЫ");
    list_append(list, "SUKA");
    foreach(list, (void (*)(void *))puts);
}
