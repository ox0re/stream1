Q=@
QQ=@

TARGET_MAIN=main
TARGET_TEST=test

SRC=src
BUILD=build
TARGET=$(BUILD)/target

SOURCES=$(wildcard $(SRC)/*.c)
OBJECTS=$(patsubst $(SRC)/%.c,$(BUILD)/%.c.o,$(SOURCES))

INCLUDE+=include
INCLUDE+=deps/Criterion/include
INCLUDE:=$(patsubst %,-I%,$(INCLUDE))

#COMMON+=-fsanitize=address

CFLAGS+=$(COMMON)
CFLAGS+=$(INCLUDE)
CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Wpedantic

LDFLAGS+=$(COMMON)

LDFLAGS_MAIN+=$(LDFLAGS)

LDFLAGS_TEST+=$(LDFLAGS)
LDFLAGS_TEST+=-Wl,-rpath=./deps/Criterion/build
LDFLAGS_TEST+=-L=./deps/Criterion/build
LDFLAGS_TEST+=-lcriterion

all:
all: $(BUILD)
all: $(TARGET)
all: $(TARGET_MAIN)
all: $(TARGET_TEST)

$(BUILD):
	mkdir -p $@

$(TARGET):
	mkdir -p $@

$(TARGET_MAIN): $(OBJECTS) $(TARGET)/$(TARGET_MAIN).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_MAIN) -o $@ $^

$(TARGET_TEST): $(OBJECTS) $(TARGET)/$(TARGET_TEST).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_TEST) -o $@ $^

deps/munit/munit.c.o: deps/munit/munit.c
	$(QQ) echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

$(BUILD)/%.c.o: $(SRC)/%.c
	$(QQ) echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

clean:
ifneq ($(wildcard $(OBJECTS))),)
	$(QQ) echo "  RM      $(OBJECTS)"
	$(Q) $(RM) $(OBJECTS)
endif
ifneq ($(wildcard $(TARGET)/$(TARGET_MAIN).c.o)),)
	$(QQ) echo "  RM      $(TARGET)/$(TARGET_MAIN).c.o"
	$(Q) $(RM) $(TARGET)/$(TARGET_MAIN).c.o
endif
ifneq ($(wildcard $(TARGET)/$(TARGET_TEST).c.o)),)
	$(QQ) echo "  RM      $(TARGET)/$(TARGET_TEST).c.o"
	$(Q) $(RM) $(TARGET)/$(TARGET_TEST).c.o
endif
ifneq ($(wildcard $(TARGET_MAIN)),)
	$(QQ) echo "  RM      $(TARGET_MAIN)"
	$(Q) $(RM) $(TARGET_MAIN)
endif
ifneq ($(wildcard $(TARGET_TEST)),)
	$(QQ) echo "  RM      $(TARGET_TEST)"
	$(Q) $(RM) $(TARGET_TEST)
endif

.PHONY: all clean
